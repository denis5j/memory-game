#ifndef MAINMENUSTATE_H
#define MAINMENUSTATE_H

#include "State.h"
#include "../../Model/Model.h"
#include "../../Model/Sound/SoundManager.h"


class MainMenuState : public State
{
public:
	MainMenuState(Model* model, SoundManager* soundManager);

	void ProcessEvent(const Event& event) override;

protected:
	Model* _model;
	SoundManager* _soundManager;
};

#endif