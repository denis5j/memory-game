#include "VictoryState.h"
#include "../../View/Event/MouseEvent.h"
#include "../../View/Event/EventManager.h"
#include "../../View/Event/ChangeStateEvent.h"

VictoryState::VictoryState(Model* model, SoundManager* soundManager) : _model(model), _soundManager(soundManager)
{
	_model->CreateVictory();
}

void VictoryState::ProcessEvent(const Event& event)
{
	if (const MouseEvent* mouseEvent = dynamic_cast<const MouseEvent*>(&event))
	{
		auto backButton = _model->GetVictory()->GetBackButton();

		if (backButton->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()))
			EventManager::AddEvent(std::make_unique<ChangeStateEvent>(2));

	}
}
