#include "GameState.h"
#include "../../View/Event/CardAnimationEvent.h"
#include "../../View/Event/MouseEvent.h"
#include "../../View/Event/MouseMotionEvent.h"
#include "../../View/Event/KeyboardEvent.h"
#include "../../View/Event/EventManager.h"
#include "../../View/Event/ChangeStateEvent.h"
#include <algorithm>

GameState::GameState(Model* model, SoundManager* soundManager) : _model(model)
{
	_soundManager = soundManager;

	_model->CreatePlayer("Player");

	_model->CreateGameScene();

}

GameState::~GameState()
{
	_model->DeletePlayers();
}

void GameState::ProcessEvent(const Event& event)
{
	if (const CardAnimationEvent* cardAnimationEvent = dynamic_cast<const CardAnimationEvent*>(&event))
	{

	}
	else if (const MouseEvent* mouseEvent = dynamic_cast<const MouseEvent*>(&event))
	{
		
	}
	else if (const KeyboardEvent* keyboardEvent = dynamic_cast<const KeyboardEvent*>(&event))
	{
		auto player = _model->GetPlayers()[0];

		auto position = player->GetPosition();

		if (keyboardEvent->GetKey() == keys::left)
		{
			player->ChangePosition(position + glm::vec2(-10, 0));
		}
		else if (keyboardEvent->GetKey() == keys::right)
		{
			player->ChangePosition(position + glm::vec2(10, 0));
		}
	}
	else if (const MouseMotionEvent* mouseMotionEvent = dynamic_cast<const MouseMotionEvent*>(&event))
	{
		auto player = _model->GetPlayers()[0];

		auto position = glm::vec2(mouseMotionEvent->GetX() - player->GetSize().x / 2.0f, player->GetPosition().y);

		player->ChangePosition(position);
	}
}
