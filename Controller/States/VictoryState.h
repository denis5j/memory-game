#ifndef VICTORYSTATE_H
#define VICTORYSTATE_H


#include "State.h"
#include "../../Model/Model.h"
#include "../../Model/Sound/SoundManager.h"


class VictoryState : public State
{
public:
	VictoryState(Model* model, SoundManager* soundManager);

	void ProcessEvent(const Event& event) override;

protected:
	Model* _model;
	SoundManager* _soundManager;
};

#endif