#include "Controller.h"
#include "States/GameState.h"
#include "States/MainMenuState.h"
#include "../View/Event/ChangeStateEvent.h"
#include "States/VictoryState.h"
#include "../Model/Updatable.h"

Controller::Controller(Model* model, View* view)
{
	_model = model;
	_view = view;

	_soundManager = std::make_unique<SoundManager>();
	///_soundManager->PlayMusic();

	_currentState = std::make_unique<GameState>(_model, _soundManager.get());
}

void Controller::OnUpdate(float deltaTime)
{
	auto events = _view->GetEvents();

	for (const auto& event : events)
		ProcessEvent(*event);

	auto updatables = Updatable::_activeUpdatabels;

	for (auto const &updatable : updatables)
		updatable->OnUpdate(deltaTime);

	_model->GetGameScene()->UpdateBricks();
}

void Controller::ProcessEvent(const Event &event)
{
	if (const ChangeStateEvent* changeStateEvent = dynamic_cast<const ChangeStateEvent*>(&event))
	{
		switch (changeStateEvent->GetState())
		{
		case states::gameState:
			_currentState = std::make_unique<GameState>(_model, _soundManager.get());
			break;
		case states::victoryState:
			_currentState = std::make_unique<VictoryState>(_model, _soundManager.get());
			break;
		case states::mainMenuState:
			_currentState = std::make_unique<MainMenuState>(_model, _soundManager.get());
			break;
		default:
			break;
		}
	}
	else
	{
		_currentState->ProcessEvent(event);
	}
}
