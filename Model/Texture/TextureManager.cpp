#include "TextureManager.h"

std::map<std::string, std::unique_ptr<Texture>> TextureManager::_mapImages;

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

Texture* TextureManager::GetTexture(const std::string& path, bool image)
{
	if (!image)
		return nullptr;

	if (_mapImages[path] == nullptr)
		LoadImageTexture(path);

	return _mapImages[path].get();
}

void TextureManager::LoadImageTexture(const std::string& path)
{
	auto texture = std::make_unique<Texture>();
	texture->CreateImageTexture(path);

	_mapImages[path] = std::move(texture);
}
