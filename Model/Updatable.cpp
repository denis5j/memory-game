#include "Updatable.h"

std::vector<Updatable*> Updatable::_activeUpdatabels;

Updatable::Updatable()
{
	_activeUpdatabels.push_back(this);
}

Updatable::~Updatable()
{
	for (int i = 0; i < _activeUpdatabels.size(); i++)
		if (_activeUpdatabels[i] == this)
			_activeUpdatabels.erase(_activeUpdatabels.begin() + i);
}
