#include "Ball.h"
#include "Brick.h"
#include "Player.h"
#include "Wall.h"
#include "../../Configuration.h"

#include "../../View/Event/EventManager.h"
#include "../../View/Event/ChangeStateEvent.h"

#include "../../View/Window.h"


#include <optional>

#define PI 3.14159265

Ball::Ball(glm::vec2 position, glm::vec2 size) : Collisionable(position, size), Updatable(), _speed(150.0f), _maxSpeed(500.0f),_increaseFactor(1.05f)
{
	SetImageTexture(Configuration::GetBackButtonPath());

	_direction = glm::normalize(glm::vec2(-0.1f, -1.065f));
}

void Ball::OnUpdate(float deltaTime)
{
	auto findLineIntersection = [this](glm::vec3 closestEdge)
	{
		glm::vec3 ballLine = glm::vec3(glm::vec2(_direction.y, -_direction.x), -glm::dot(glm::vec2(_direction.y, -_direction.x), _centerPosition));

		glm::vec3 interPointVec3 = glm::cross(ballLine, closestEdge);

		auto intersectionPoint = glm::vec2(interPointVec3 / interPointVec3.z);

		return intersectionPoint;
	};
	
	auto presjekpravcakruznice = [this](glm::vec2 center, glm::vec2 direction, glm::vec2 edgeVertex, float r)
	{
		float a = glm::length(direction) * glm::length(direction);
		float b = -2.0f * glm::dot((edgeVertex - center), direction);
		float c = glm::length(edgeVertex - center) * glm::length(edgeVertex- center) - r * r;
		float disc = b * b - 4.0f * a * c;

		if (disc >= 0)
		{
			float t1 = (-b + sqrt(disc)) / (2.0f * a);
			float t2 = (-b - sqrt(disc)) / (2.0f * a);
			auto point1 = center + direction * t1;
			auto point2 = center + direction * t2;
			return std::pair<glm::vec2, glm::vec2>(point1, point2);
		}

		return std::pair<glm::vec2, glm::vec2>(glm::vec2(), glm::vec2());
	};

	auto presjekPravcaIElipse = [this](glm::vec2 pointOnLine, glm::vec2 lineDirection, float a, float b)
	{
		float aa = (lineDirection.x * lineDirection.x) / (a * a);
		aa += (lineDirection.y * lineDirection.y) /( b * b);
		float bb = (2.0f * pointOnLine.x * lineDirection.x) / (a * a);
		bb += (2.0f * pointOnLine.y * lineDirection.y) / (b * b);
		float cc = pointOnLine.x * pointOnLine.x / (a * a);
		cc += pointOnLine.y * pointOnLine.y / (b * b);
		cc -= 1.0f;
		float disc = bb * bb - 4.0f * aa * cc;

		if (disc >= 0)
		{
			float t1 = (-bb + sqrt(disc)) / (2.0f * aa);
			float t2 = (-bb - sqrt(disc)) / (2.0f * aa);
			auto point1 = pointOnLine + lineDirection * t1;
			auto point2 = pointOnLine + lineDirection * t2;

			return glm::dot(point1, lineDirection) < glm::dot(pointOnLine, lineDirection) ? point1 : point2;
		}

		return glm::vec2();
	};



	auto findCenterIntersectionPoint = [this, findLineIntersection, presjekpravcakruznice](glm::vec3 closestEdge, Collisionable* colidible)
	{
		std::pair<glm::vec2, glm::vec2> result;//centar i tocka dodira

		auto edgeBallLineIntersection = findLineIntersection(closestEdge);

		auto normal = glm::vec3(glm::vec2(closestEdge.y, -closestEdge.x), -glm::dot(glm::vec2(closestEdge.y, -closestEdge.x), _centerPosition));

		glm::vec3 projPointVec3 = glm::cross(normal, closestEdge);

		auto projectionPoint = glm::vec2(projPointVec3 / projPointVec3.z);

		auto distanceTilIntersection = glm::length(edgeBallLineIntersection - _centerPosition);

		auto distanceVertical = glm::length(projectionPoint - _centerPosition);

		float halfSize = _drawingSize.x / 2.0f;

		auto littleDistance = distanceTilIntersection * halfSize / distanceVertical;

		auto centerIntersectionPoint = edgeBallLineIntersection - littleDistance * _direction;

		normal = glm::vec3(glm::vec2(closestEdge.y, -closestEdge.x), -glm::dot(glm::vec2(closestEdge.y, -closestEdge.x), centerIntersectionPoint));

		projPointVec3 = glm::cross(normal, closestEdge);

		projectionPoint = glm::vec2(projPointVec3 / projPointVec3.z);

		result.second = projectionPoint;

		if (!colidible->IsInrange(projectionPoint.x, projectionPoint.y))
		{
			glm::vec2 edgePoint = glm::vec2();

			std::vector<glm::vec2> edgePoints;

			for (auto edge : colidible->GetEdges())
			{
				if (glm::dot(glm::vec2(edge), glm::vec2(closestEdge)) == 0.0f)
				{
					auto point = glm::cross(edge, closestEdge);

					edgePoints.push_back(glm::vec2(point) / point.z);
				}
			}

			edgePoint = glm::length(edgePoints[0] - centerIntersectionPoint) < glm::length(edgePoints[1] - centerIntersectionPoint) ? edgePoints[0] : edgePoints[1];

			result.second = edgePoint;

			auto points = presjekpravcakruznice(_centerPosition, _direction, edgePoint, _drawingSize.x / 2.0f);

			if (points.first == glm::vec2())
				return std::pair<glm::vec2,glm::vec2>();

			centerIntersectionPoint = glm::dot(points.first, _direction) < glm::dot(points.second, _direction) ? points.first : points.second;
		}

		result.first = centerIntersectionPoint;

		return result;
	};

	auto findClosestEdge = [this, findCenterIntersectionPoint](std::vector<glm::vec3> edges, Collisionable* colidible)
	{
		glm::vec3 closestEdge = glm::vec3();

		double min = DBL_MAX;

		for (auto edge : edges)
		{
			auto intersectionPoint = findCenterIntersectionPoint(edge, colidible).first;
	
			double distance = glm::distance(intersectionPoint, _centerPosition);
			
			if (distance < min)
			{
				closestEdge = edge;
				min = distance;
			}
		}

		return closestEdge;
	};


	float minTimeTillCrash = FLT_MAX;

	Collisionable* closestColidible = nullptr;

	std::pair<glm::vec2, glm::vec2> pointandnormal;

	for (auto colidible : _activeCollisionables)
	{
		if (colidible == dynamic_cast<Ball*>(this))
			continue;

		//auto cosfi = glm::dot(glm::normalize(_direction), glm::normalize(colidible->GetCenter() - GetCenter()));

		//auto distance = glm::distance(_centerPosition, colidible->GetCenter());

		//if (cosfi <= cos(0.15f) && distance > 400.0f)
		//	continue;

		//if (!Contains(colidible))
		//	continue;

		auto isCollisionPossible = [this](Collisionable* collisionable)
		{
			auto centardot = glm::dot(_centerPosition, _direction);

			for (auto edge : collisionable->GetEdge())
			{
				if (centardot < glm::dot(edge->GetStartPoint(), _direction))
					return true;
			}

			//std::vector<glm::vec2> _vertex;
			//_vertex.push_back(collisionable->GetPosition());
			//_vertex.push_back(collisionable->GetPosition() + glm::vec2(collisionable->GetSize().x, 0.0f));
			//_vertex.push_back(collisionable->GetPosition() + glm::vec2(0.0f, collisionable->GetSize().y));
			//_vertex.push_back(collisionable->GetPosition() + collisionable->GetSize());


			//for (auto vertex : _vertex) 
			//{
			//	if (centardot < glm::dot(vertex, _direction))
			//		return true;
			//}

			return false;
		};

		if (!isCollisionPossible(colidible))
			continue;

		auto edges = colidible->GetEdges();

		glm::vec3 closestEdge = findClosestEdge(edges, colidible);
		
		if (closestEdge == glm::vec3())
			continue;

		auto pointnormal = findCenterIntersectionPoint(closestEdge, colidible);

		auto intersectionPointCenter = pointnormal.first;

		float distanceTilCrash = glm::length(intersectionPointCenter - _centerPosition);

		float timeTilCrash = distanceTilCrash / _speed;

		if (timeTilCrash < minTimeTillCrash)
		{
			minTimeTillCrash = timeTilCrash;

			closestColidible = colidible;

			pointandnormal = pointnormal;
		}
	}

	if (minTimeTillCrash < deltaTime)
	{
		auto deltaPosition = minTimeTillCrash * _direction * _speed;

		_drawingPosition += deltaPosition;
		_centerPosition += deltaPosition;

		deltaTime -= minTimeTillCrash;

		auto edges = closestColidible->GetEdges();

		glm::vec3 closestEdge = findClosestEdge(edges, closestColidible);

		auto normal = pointandnormal.second - pointandnormal.first;
		normal = glm::vec2(-normal.y, normal.x);

		if (auto player = dynamic_cast<Player*>(closestColidible))
		{
			auto playerHalfSize = player->GetSize() / 2.0f;
			auto playerTopCenter = player->GetCenter() - glm::vec2(0.0f, playerHalfSize.y);

			auto intersectionPoint = pointandnormal.first;
			intersectionPoint -= playerTopCenter;

			auto point = presjekPravcaIElipse(intersectionPoint, _direction, playerHalfSize.x, playerHalfSize.y);

			if (point != glm::vec2())
			{
				point += playerTopCenter;

				normal = glm::vec2(2 * (point.x - playerTopCenter.x) / (playerHalfSize.x * playerHalfSize.x), 2 * (point.y - playerTopCenter.y) / (playerHalfSize.y * playerHalfSize.y));

				normal = glm::vec2(-normal.y, normal.x);
			}

			
		}

		float angle = atan2(_direction.y, _direction.x) - atan2(normal.y, normal.x);

		glm::mat2x2 rotationMatrix = { cos(-2 * angle),-sin(-2 * angle),sin(-2 * angle),cos(-2 * angle) };

		_direction = _direction * rotationMatrix;

		if (auto brick = dynamic_cast<Brick*>(closestColidible))
			brick->Hit();

		_speed *= _increaseFactor;

		if (_speed > _maxSpeed)
			_speed = _maxSpeed;
	}

	auto deltaPosition = deltaTime * _direction * _speed;

	_drawingPosition += deltaPosition;
	_centerPosition += deltaPosition;

	if (_drawingPosition.y > Window::GetHeight())
	{
		EventManager::AddEvent(std::make_unique<ChangeStateEvent>(1));
		exit(0);
	}
}

std::vector<glm::vec3> Ball::GetEdges()
{
	return std::vector<glm::vec3>();
}
