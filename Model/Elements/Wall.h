#ifndef WALL_H
#define WALL_H

#include "../Collisionable.h"

class Wall : public Collisionable
{
public:
	Wall(glm::vec2 position, glm::vec2 size, glm::vec2 directions);

	std::vector<glm::vec3> GetEdges();
	std::vector<Edge*> GetEdge();
protected:
	glm::vec2 _point;
	glm::vec2 _direction;

	std::vector<glm::vec3> _edges;
};

#endif