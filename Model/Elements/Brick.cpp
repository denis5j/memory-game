#include "Brick.h"
#include "../Sound/SoundManager.h"

Brick::Brick(char id, std::string texturePath, int hitPoints, std::string hitSoundPath, int breakScore, std::string breakSoundPath, glm::vec2 position, glm::vec2 size, bool infinitePoints) : Collisionable(position, size), _hitPoints(hitPoints), _breakScore(breakScore), _infinitePoints(infinitePoints)
{
	_id = id;

	_texturePath = texturePath;
	_hitSoundPath = hitSoundPath;
	_breakSoundPath = breakSoundPath;

	SetImageTexture(texturePath);

	auto direction = glm::vec2(0.0f, -1.0f);
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position + size)));
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position)));

	direction = glm::vec2(-1.0f, 0.0f);
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position + size)));
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position)));
}

void Brick::SetId(char id)
{
	_id = id;
}

void Brick::SetHitPoints(int hitPoints)
{
	_hitPoints = hitPoints;
}

void Brick::SetBreakScore(int breakScore)
{
	_breakScore = breakScore;
}

void Brick::SetTexturePath(std::string path)
{
	_texturePath = path;
}

void Brick::Hit()
{
	if (!_infinitePoints)
		_hitPoints -= 1;

	if(_hitPoints)
		SoundManager::PlaySound(_hitSoundPath);
	else
		SoundManager::PlaySound(_breakSoundPath);
}

int Brick::GetHitPoints() const
{
	return _hitPoints;
}

std::vector<glm::vec3> Brick::GetEdges()
{
	return _edges;
}
