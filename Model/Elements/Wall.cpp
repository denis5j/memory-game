#include "Wall.h"
#include "../../Configuration.h"

Wall::Wall(glm::vec2 position, glm::vec2 size, glm::vec2 directions) : Collisionable(position, size), _point(position), _direction(directions)
{
	SetImageTexture(Configuration::GetCardBackPath());

	auto direction = glm::vec2(0.0f, -1.0f);
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position + size)));
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position)));

	direction = glm::vec2(-1.0f, 0.0f);
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position + size)));
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position)));
}

std::vector<glm::vec3> Wall::GetEdges()
{
	return _edges;
}

std::vector<Edge*> Wall::GetEdge()
{
	return std::vector<Edge*>();
}
