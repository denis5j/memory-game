#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "../Drawable.h"

class Background : public Drawable
{
public:
	Background(glm::vec2 position, glm::vec2 size);

protected:
};

#endif