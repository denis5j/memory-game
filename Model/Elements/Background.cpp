#include "Background.h"
#include "../../Configuration.h"

Background::Background(glm::vec2 position, glm::vec2 size) : Drawable(position, size)
{
	SetImageTexture(Configuration::GetBackgroundPath());
}
