#include "Text.h"

#define PI 3.14159265

Text::Text(glm::vec2 position, std::string text, int fontSize, double angle, bool centered) : Drawable(position, glm::vec2(), angle), _position(position), _fontSize(fontSize)
{
	_r = 209;
	_g = 78;
	_b = 2;
	_centered = centered;

	SetText(text, fontSize);
}

void Text::SetText(std::string text, int fontSize)
{
	SetTextTexture(text, fontSize == -1 ? _fontSize : fontSize, _r, _g, _b);

	if(_centered)
	{
		glm::mat2x2 rotationMatrix = { cos(_angle * PI / 180.0f), -sin(_angle * PI / 180.0f), sin(_angle * PI / 180.0f), cos(_angle * PI / 180.0f) };

		glm::vec2 deltaPosition = rotationMatrix * _drawingSize;

		_drawingPosition = _position - deltaPosition;
	}
}

void Text::SetColor(int r, int g, int b)
{
	_r = r;
	_g = g;
	_b = b;
}