#ifndef BRICK_H
#define BRICK_H

#include "../Collisionable.h"

class Brick : public Collisionable
{
public:
	Brick(char id, std::string texturePath, int hitPoints, std::string hitSoundPath, int breakScore, std::string breakSoundPath, glm::vec2 position, glm::vec2 size, bool infinitePoints = false);

	void SetId(char id);
	void SetHitPoints(int hitPoints);
	void SetBreakScore(int breakScore);
	void SetTexturePath(std::string path);

	void Hit();

	int GetHitPoints() const;

	std::vector<glm::vec3> GetEdges();

protected:
	bool _infinitePoints;
	char _id;
	int _hitPoints;
	int _breakScore;
	std::string _texturePath;
	std::string _hitSoundPath;
	std::string _breakSoundPath;

	std::vector<glm::vec3> _edges;
};

#endif