#include "VictoryMenu.h"
#include "../../Configuration.h"

VictoryMenu::VictoryMenu(std::vector<Player*> players)
{
	//_victory = std::make_unique<Text>(600, 50, "Victory", 80);
	//_top20 = std::make_unique<Text>(800, 170, "Top 20", 40);
	//_winers = std::make_unique<Text>(400, 170, "Winer", 40);
	//_backButton = std::make_unique<Button>(600 - 150, 850, 300, 100, Configuration::GetBackButtonPath());

	CreateWinnersAndRangList(players);
}

void VictoryMenu::SortPlayersByPoints(std::vector<Player*> &players) const
{
	for (int i = 0; i < players.size(); i++)
		for (int j = i + 1; j < players.size(); j++)
			if (players[i]->GetPoints() < players[j]->GetPoints())
				std::swap(players[i], players[j]);
}

Button* VictoryMenu::GetBackButton() const
{
	return _backButton.get();
}

void VictoryMenu::CreateWinnersAndRangList(std::vector<Player*> &players)
{
	/*int playerPointsY = 230;
	int victoryPlayersY = 230;

	SortPlayersByPoints(players);

	int position = 1;
	int currentPoints = players[0]->GetPoints();

	for (auto& players : players)
	{
		if (_playerPoints.size() == 20)
			return;

		if (currentPoints != players->GetPoints())
			position++;

		currentPoints = players->GetPoints();

		_playerPoints.push_back(std::make_unique<Text>(800, playerPointsY, std::to_string(position) + ". " + players->GetName() + " " + std::to_string(players->GetPoints()), 25));
		playerPointsY += _playerPoints[_playerPoints.size() - 1].get()->GetSizeY();

		if (position != 1)
			continue;

		_victoryPlayers.push_back(std::make_unique<Text>(400, victoryPlayersY, players->GetName(), 25));
		victoryPlayersY += _victoryPlayers[_victoryPlayers.size() - 1].get()->GetSizeY();

	}

	if (_victoryPlayers.size() > 1)
		_winers = std::make_unique<Text>(400, 170, "Winers", 40);*/
}
