#ifndef GAME_SCENE_H
#define GAME_SCENE_H

#include <vector>
#include <memory>

#include "../XMLParser.h"

#include "../Elements/Ball.h"
#include "../Elements/Wall.h"
#include "../Elements/Brick.h"
#include "../Elements/Text.h"
#include "../Elements/Player.h"
#include "MainMenu.h"

class GameScene
{
public:
	GameScene(std::vector<Player*> players, std::unique_ptr<MainMenu> mainMenu);
	~GameScene();

	void CrateGameText(std::vector<Player*> players);

	void SetCurrentPlayerPointsText(Player* player);

	void CreateBall();
	void CreateWalls();
	void CreateBricks(int rowCount, int colCount, int rowSpacing, int colSpacing);
	void LoadBricks();

	glm::vec2 CalaculateBrickSize(int rowCount, int colCount, int rowSpacing, int colSpacing);

	void UpdateBricks();

protected:
	int _currentPlayerPosition;

	std::vector<Player*> _players;

	std::unique_ptr<Ball> _ball;
	std::vector<std::unique_ptr<Wall>> _walls;
	std::vector<std::unique_ptr<Brick>> _bricks;
	std::unique_ptr<Text> _currentPlayerPointsText;
	std::vector<std::unique_ptr<Text>> _playerNameTexts;

	std::unique_ptr<XMLParser> _xmlParser;
};

#endif