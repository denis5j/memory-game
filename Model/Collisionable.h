#ifndef COLLISIONABLE_H
#define COLLISIONABLE_H

#include "Drawable.h"
#include "Elements/Edge.h"

#include <vector>

class Collisionable : public Drawable
{
public:
	Collisionable(glm::vec2 position, glm::vec2 size);
	virtual ~Collisionable();

	static std::vector<Collisionable*> _activeCollisionables;

	virtual std::vector<glm::vec3> GetEdges() = 0;

	std::vector<Edge*> GetEdge();

protected:

	std::vector<std::unique_ptr<Edge>> _edge;
};

#endif