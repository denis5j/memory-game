#ifndef UPDATABLE_H
#define UPDATABLE_H

#include <vector>

class Updatable
{
public:
	Updatable();
	virtual ~Updatable();

	virtual void OnUpdate(float deltaTime) = 0;

	static std::vector<Updatable*> _activeUpdatabels;

protected:

};

#endif