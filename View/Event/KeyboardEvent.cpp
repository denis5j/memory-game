#include "KeyboardEvent.h"

KeyboardEvent::KeyboardEvent(keys key) : Event(), _key(key)
{
}

keys KeyboardEvent::GetKey() const
{
	return _key;
}

