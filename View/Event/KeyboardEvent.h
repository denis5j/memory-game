#ifndef KEYBOARD_EVENT_H
#define KEYBOARD_EVENT_H

#include "Event.h"

enum class keys {
	left, right
};

class KeyboardEvent : public Event
{
public:
	KeyboardEvent(keys key);

	keys GetKey() const;

protected:
	keys _key;
};

#endif