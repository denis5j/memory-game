#include "EventManager.h"
#include "MouseEvent.h"
#include "MouseMotionEvent.h"
#include "KeyboardEvent.h"
#include <SDL.h>

std::vector<std::unique_ptr<Event>> EventManager::_events;

EventManager::EventManager()
{
}
#include <iostream>
std::vector<std::unique_ptr<Event>> EventManager::GetEvents() const
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_MOUSEMOTION:
			OnMouseMotion(event);
			break;
		case SDL_MOUSEBUTTONDOWN:
			OnMouseButtonDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			OnMouseButtonUp(event);
			break;
		case SDL_QUIT:
			exit(0);
			break;
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_LEFT)
				OnKeyboardLeftButton(event);
			if (event.key.keysym.sym == SDLK_RIGHT)
				OnKeyboardRightButton(event);
		default:
			break;
		}
	}
	
	return std::move(_events);
}

void EventManager::OnKeyboardLeftButton(SDL_Event const& event) const
{
	_events.push_back(std::make_unique<KeyboardEvent>(keys::left));
}

void EventManager::OnKeyboardRightButton(SDL_Event const& event) const
{
	_events.push_back(std::make_unique<KeyboardEvent>(keys::right));
}

void EventManager::OnMouseButtonDown(SDL_Event const& event) const
{
	auto x = event.motion.x;
	auto y = event.motion.y;

	auto button = event.button.button;
	auto state = 1;

	_events.push_back(std::make_unique<MouseEvent>(x, y, button, state));
}

void EventManager::OnMouseButtonUp(SDL_Event const& event) const
{
	auto x = event.motion.x;
	auto y = event.motion.y;

	auto button = event.button.button;
	auto state = 0;

	_events.push_back(std::make_unique<MouseEvent>(x, y, button, state));
}

void EventManager::OnMouseMotion(SDL_Event const& event) const
{
	auto x = event.motion.x;
	auto y = event.motion.y;

	auto button = event.button.button;
	auto state = 0;

	_events.push_back(std::make_unique<MouseMotionEvent>(x, y, button, state));
}

void EventManager::AddEvent(std::unique_ptr<Event> event)
{
	_events.push_back(std::move(event));
}
