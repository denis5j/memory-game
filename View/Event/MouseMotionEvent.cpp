#include "MouseMotionEvent.h"

MouseMotionEvent::MouseMotionEvent(int x, int y, int button, int state) : Event(), _x(x), _y(y), _button(button), _state(state)
{
}

int MouseMotionEvent::GetX() const
{
	return _x;
}

int MouseMotionEvent::GetY() const
{
	return _y;
}

int MouseMotionEvent::GetButton() const
{
	return _button;
}

int MouseMotionEvent::GetState() const
{
	return _state;
}

