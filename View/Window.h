#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include <memory>

struct SDL_Renderer;
struct SDL_Window;
struct SDL_Surface;

struct SDL_Renderer_Deleter
{
	void operator() (SDL_Renderer* const p) const noexcept;
};

struct SDL_Window_Deleter
{
	void operator() (SDL_Window* const p) const noexcept;
};

class Window
{
public:
	Window(std::string title, int width, int height);

	static SDL_Renderer* GetRenderer();
	static int GetWidth();
	static int GetHeight();
	static void ClearRenderer();
	static void UpdateWindow();

protected:
	static std::unique_ptr<SDL_Renderer, SDL_Renderer_Deleter> _renderer;
	std::unique_ptr<SDL_Window, SDL_Window_Deleter> _window;

	SDL_Surface* _surface;

	std::string _windowTitle;
	static int _windowWidth;
	static int _windowHeight;
};

#endif