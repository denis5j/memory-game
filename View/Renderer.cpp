#include "Renderer.h"
#include "Window.h"

#include <SDL.h>

void Renderer::Render(Drawable const* const drawable) const
{
	SDL_SetRenderDrawColor(Window::GetRenderer(), 0x3A, 0xA8, 0x36, 0xFF);

	auto position = drawable->GetPosition();
	auto size = drawable->GetSize();
	auto rotation = drawable->GetRotation();

	SDL_Rect texture_rect;
	texture_rect.x = position.x;
	texture_rect.y = position.y;
	texture_rect.w = size.x;
	texture_rect.h = size.y;

	SDL_Point point;
	point.x = rotation.x;
	point.y = rotation.y;

	SDL_RenderCopyEx(Window::GetRenderer(), drawable->GetTexture(), NULL, &texture_rect, drawable->GetAngle(), &point, SDL_FLIP_NONE);
}

void Renderer::ClearRenderer() const
{
	Window::ClearRenderer();
}
