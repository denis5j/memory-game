#ifndef RENDERER_H
#define RENDERER_H

#include "../Model/Drawable.h"

class Renderer
{
public:
	void Render(Drawable const* const drawable) const;

	void ClearRenderer() const;
protected:
};

#endif